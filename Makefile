.DEFAULT_GOAL := top

SYNTH_SOURCES := \
	src/macros.sv \
	src/synchronizer.sv \
	src/ram_1r1w_sync.sv \
	src/SpiBus.sv \
	src/SpiStage.sv \
	src/UartBaudTickGenerator.sv \
	src/UartSink.sv \
	src/CameraController.sv \
	src/Firmware.sv \
	src/Cpu.sv \
	src/top.sv
NONSYNTH_SOURCES := \
	src/test_SpiBus.sv \
	src/test_top.sv
SOURCES := ${SYNTH_SOURCES} ${NONSYNTH_SOURCES}

.PHONY : top
top :
	iverilog \
		-g2005-sv \
		-o simulate \
		${SOURCES:%=-l %} \
		-s top

.PHONY : test-top
test-top :
	iverilog \
		-g2005-sv \
		-o simulate \
		${SOURCES:%=-l %} \
		-s test_top

.PHONY : test-spi
test-spi :
	iverilog \
		-g2005-sv \
		-o simulate \
		${SOURCES:%=-l %} \
		-s test_SpiBus

.PHONY : prog
prog : top.bin
	iceprog top.bin


top.json : ${SYNTH_SOURCES}
	yosys \
		-ql top.yslog \
		-p 'synth_ice40 -top top -json top.json' \
		${SYNTH_SOURCES}

top.asc : top.json
	nextpnr-ice40 \
		-ql top.nplog \
		--up5k \
		--package sg48 \
		--freq 12 \
		--asc top.asc \
		--pcf icebreaker.pcf \
		--json top.json \
		--top top

top.bin : top.asc
	icepack top.asc top.bin
