// A pipeline stage around a single SPI peripheral.
module SpiStage #(
  parameter  integer LOG_WIDTH = 3,  // 2**3 = 8,
  localparam integer WIDTH = 2 ** LOG_WIDTH
) (
  input  wire logic clk_i,
  input  wire logic reset_i,

  // Peripheral interface
  output wire logic sclk_o,
  output wire logic nselect_o,
  output wire logic outbound_o,
  input  wire logic inbound_i,

  // Upstream interface
  input  wire logic valid_i,
  output wire logic ready_o,
  input  wire logic[WIDTH-1:0] data_i,
  // TODO: Add `addr_i` to identify one of many SPI peripherals.

  // Downstream interface
  output wire logic valid_o,
  input  wire logic ready_i,
  output wire logic[WIDTH-1:0] data_o
);
  wire logic idle_o;
  SpiBus#(LOG_WIDTH) spi(
    .clk_i(clk_i),
    .reset_i(reset_i),

    // Low-level interface
    .sclk_o(sclk_o),
    .nselect_o(nselect_o),
    .outbound_o(outbound_o),
    .inbound_i(inbound_i),

    // High-level interface
    .valid_i(ready_o & valid_i),
    .ready_o(idle_o),
    .data_i(data_i),
    .data_o(data_o)
  );

  var logic busy_r, busy_n;
  always_ff @(posedge clk_i)
    busy_r <= (reset_i) ? 0 : busy_n;
  always_comb
    case (busy_r)
      // We remain idle until we receive data from upstream.
      0: busy_n =  (ready_o & valid_i);

      // We remain busy until both:
      // - Our current word of data is being sent downstream, and
      // - No more upstream data is forthcoming.
      1: busy_n = ~(ready_i & valid_o) | valid_i;
    endcase

  assign
    // We're ready for new data if the SPI interface is idle,
    // and either we don't have any data or downstream is about to take it.
    ready_o = idle_o & (~busy_r | ready_i),

    // We have valid data if we've completed an exchange
    // and the resulting data hasn't been consumed yet.
    valid_o = idle_o &  busy_r;
endmodule
