// I don't like this either, but:
// * IVerilog doesn't like ternaries `(cond) ? x : y`
//   where `x` and `y` are of a `typedef`-introduced type
// * Yosys doesn't like casts `Ty'(expr)`
//   where `Ty` is a `typedef`-introduced type
`ifdef SYNTHESIS
`define MUX_CAST(Ty, Expr) (Expr)
`else
`define MUX_CAST(Ty, Expr) Ty'(Expr)
`endif
