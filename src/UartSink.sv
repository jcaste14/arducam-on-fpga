/*
 *  icebreaker examples - Async uart tx module
 *
 *  Copyright (C) 2018 Piotr Esden-Tempski <piotr@esden.net>
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
// lightly restyled by Jonathan Castello (2023)
module UartSink #(
  parameter integer CLOCK_FREQ = 12_000_000,
  parameter integer BAUD_RATE  = 115_200
) (
  input  wire logic clk_i,
  input  wire logic reset_i,

  // Low-level peripheral interface
  output wire logic tx_o,

  // High-level pipeline interface
  input  wire logic[7:0] data_i,
  input  wire logic      valid_i,
  output wire logic      ready_o
);
  wire logic bit_tick_i;
  UartBaudTickGenerator #(
    .CLOCK_FREQ(CLOCK_FREQ),
    .BAUD_RATE(BAUD_RATE),
    .OVERSAMPLE('1)
  ) tickgen(
    .clk_i(clk_i),
    .reset_i(reset_i),
    .enable_i(~ready_o),
    .tick_o(bit_tick_i)
  );

  typedef enum logic[3:0] {
    IDLE      = 4'b0000, // tx_o = high
    BIT_START = 4'b0100, // tx_o = low
    BIT0      = 4'b1000, // tx_o = data bit 0
    BIT1      = 4'b1001, // tx_o = data bit 1
    BIT2      = 4'b1010, // tx_o = data bit 2
    BIT3      = 4'b1011, // tx_o = data bit 3
    BIT4      = 4'b1100, // tx_o = data bit 4
    BIT5      = 4'b1101, // tx_o = data bit 5
    BIT6      = 4'b1110, // tx_o = data bit 6
    BIT7      = 4'b1111, // tx_o = data bit 7
    BIT_STOP1 = 4'b0010, // tx_o = high
    BIT_STOP2 = 4'b0011  // tx_o = high
  } State;

  var  State      state_r, state_n;
  var  logic[7:0] buf_r,   buf_n;
  always_ff @(posedge clk_i) begin
    buf_r   <= (reset_i) ? '0   : buf_n;
    state_r <= `MUX_CAST(State, (reset_i) ? IDLE : state_n);
  end
  always_comb begin
    // Defaults
    state_n = state_r;
    buf_n   = buf_r;

    if (ready_o & valid_i) begin
      buf_n = data_i;
    end else if (state_r[3] & bit_tick_i) begin
      buf_n = (buf_r >> 1);
    end

    case (state_r)
      IDLE:      if(valid_i)    state_n = BIT_START;
      BIT_START: if(bit_tick_i) state_n = BIT0;
      BIT0:      if(bit_tick_i) state_n = BIT1;
      BIT1:      if(bit_tick_i) state_n = BIT2;
      BIT2:      if(bit_tick_i) state_n = BIT3;
      BIT3:      if(bit_tick_i) state_n = BIT4;
      BIT4:      if(bit_tick_i) state_n = BIT5;
      BIT5:      if(bit_tick_i) state_n = BIT6;
      BIT6:      if(bit_tick_i) state_n = BIT7;
      BIT7:      if(bit_tick_i) state_n = BIT_STOP1;
      BIT_STOP1: if(bit_tick_i) state_n = BIT_STOP2;
      BIT_STOP2: if(bit_tick_i) state_n = IDLE;
      default:   if(bit_tick_i) state_n = IDLE;
    endcase
  end

  assign
    //         high if state IDLE, BIT_STOP1, BIT_STOP2
    //         |                high if transmitting bits
    //         |                |
    //         V                V
    tx_o    = (state_r < 4) | (state_r[3] & buf_r[0]),
    ready_o = (state_r == 0);
endmodule
