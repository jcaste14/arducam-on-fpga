/*
 *  icebreaker examples - Async uart baud tick generator module
 *
 *  Copyright (C) 2018 Piotr Esden-Tempski <piotr@esden.net>
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
// lightly restyled by Jonathan Castello (2023)
module UartBaudTickGenerator #(
  parameter integer CLOCK_FREQ = 12_000_000,
  parameter integer BAUD_RATE  = 115_200,
  parameter integer OVERSAMPLE = 1
) (
  input  wire logic clk_i,
  input  wire logic reset_i,

  input  wire logic enable_i,
  output wire logic tick_o
);
  // +/- 2% max timing error over a byte
  localparam integer ACC_WIDTH
    = $clog2(CLOCK_FREQ / BAUD_RATE) + 8;

  // make sure INCREMENT calculation doesn't overflow
  // (verilog uses 32bit variables internally)
  localparam integer SHIFT_LIMITER
    = $clog2((BAUD_RATE * OVERSAMPLE) >> (31 - ACC_WIDTH));

  // Calculate accumulate increment
  localparam integer INCREMENT
    = ( ( (BAUD_RATE * OVERSAMPLE << (ACC_WIDTH - SHIFT_LIMITER))
        + (CLOCK_FREQ >> (SHIFT_LIMITER + 1))
        )
      / (CLOCK_FREQ >> SHIFT_LIMITER)
      );

  var logic[ACC_WIDTH:0] acc_r, acc_n;
  always_ff @(posedge clk_i)
    acc_r <= (reset_i) ? '0 : acc_n;
  always_comb begin
    if (enable_i) begin
      acc_n = acc_r[ACC_WIDTH-1:0] + INCREMENT[ACC_WIDTH:0];
    end else begin
      acc_n = INCREMENT[ACC_WIDTH:0];
    end
  end
  assign
    tick_o = acc_r[ACC_WIDTH];
endmodule
