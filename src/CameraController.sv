module CameraController(
  input  wire logic clk_i,
  input  wire logic reset_i,

  // SPI request interface
  output var  logic      spi_req_valid_o,
  input  wire logic      spi_req_ready_i,
  output var  logic[7:0] spi_req_data_o,

  // SPI response interface
  input  wire logic      spi_res_valid_i,
  output var  logic      spi_res_ready_o,
  input  wire logic[7:0] spi_res_data_i,

  // Service request interface
  input  wire logic[15:0] request_data_i,
  input  wire logic       request_burst_i,
  input  wire logic[23:0] request_burst_length_i,
  input  wire logic       request_valid_i,
  output var  logic       request_ready_o,

  // Service response interface
  output var  logic[7:0]  response_data_o,
  output var  logic       response_valid_o,
  input  wire logic       response_ready_i,

  // Downstream burst output interface
  output var  logic[7:0] burst_data_o,
  output var  logic      burst_valid_o,
  input  wire logic      burst_ready_i
);
  typedef enum {
    HOLD,
    DROP,
    FEED
  } OutputState;

  var OutputState out_state_r, out_state_n;
  var logic[23:0] feed_rem_r,  feed_rem_n;
  var logic       burst_r,     burst_n;
  always_ff @(posedge clk_i) begin
    out_state_r <= `MUX_CAST(OutputState, (reset_i) ? HOLD : out_state_n);
    feed_rem_r  <= (reset_i) ? '0   : feed_rem_n;
    burst_r     <= (reset_i) ? '0   : burst_n;
  end
  // Output routing
  always_comb case (out_state_r)
    /* HOLD: */
    default: begin
      spi_res_ready_o  = '0;

      response_data_o  = '0;
      response_valid_o = '0;

      burst_data_o     = '0;
      burst_valid_o    = '0;
    end

    DROP: begin
      spi_res_ready_o  = '1;

      response_data_o  = '0;
      response_valid_o = '0;

      burst_data_o     = '0;
      burst_valid_o    = '0;
    end

    FEED: begin
      if (burst_r) begin
        spi_res_ready_o  = burst_ready_i;

        response_data_o  = '0;
        response_valid_o = '0;

        burst_data_o     = spi_res_data_i;
        burst_valid_o    = spi_res_valid_i;
      end else begin
        spi_res_ready_o  = response_ready_i;

        response_data_o  = spi_res_data_i;
        response_valid_o = spi_res_valid_i;

        burst_data_o     = '0;
        burst_valid_o    = '0;
      end
    end
  endcase
  // Output control logic
  always_comb begin
    // Defaults
    feed_rem_n  = feed_rem_r;
    burst_n     = burst_r;
    out_state_n = out_state_r;

    case (out_state_r)
      HOLD: begin
        if (request_valid_i && request_ready_o) begin
          feed_rem_n  = (request_burst_i) ? request_burst_length_i : 24'd1;
          burst_n     = request_burst_i;
          out_state_n = DROP;
        end
      end

      DROP: begin
        if (spi_res_valid_i && spi_res_ready_o) begin
          feed_rem_n  = feed_rem_r;
          burst_n     = (feed_rem_n == 0) ? '0   : burst_r;
          out_state_n = `MUX_CAST(OutputState, (feed_rem_n == 0) ? HOLD : FEED);
        end
      end

      FEED: begin
        if (spi_res_valid_i && spi_res_ready_o) begin
          feed_rem_n  = feed_rem_r - 1;
          burst_n     = (feed_rem_n == 0) ? '0   : burst_r;
          out_state_n = `MUX_CAST(OutputState, (feed_rem_n == 0) ? HOLD : FEED);
        end
      end
    endcase
  end


  typedef enum {
    IDLE,
    REQ,
    PUMP
  } InputState;

  var InputState  in_state_r, in_state_n;
  var logic[7:0]  exchange_r, exchange_n;
  var logic[24:0] pump_rem_r, pump_rem_n;
  always_ff @(posedge clk_i) begin
    in_state_r <= `MUX_CAST(InputState, (reset_i) ? IDLE : in_state_n);
    exchange_r <= (reset_i) ? '0   : exchange_n;
    pump_rem_r <= (reset_i) ? '0   : pump_rem_n;
  end
  // Input routing
  always_comb case (in_state_r)
    /* IDLE: */
    default: begin
      spi_req_valid_o = request_valid_i;
      spi_req_data_o  = request_data_i[15:7];
      request_ready_o = spi_req_ready_i;
    end

    REQ: begin
      spi_req_valid_o = '1;
      spi_req_data_o  = exchange_r;
      request_ready_o = '0;
    end

    PUMP: begin
      spi_req_valid_o = '1;
      spi_req_data_o  = '0;
      request_ready_o = '0;
    end
  endcase
  // Input control logic
  always_comb begin
    // Defaults
    exchange_n = exchange_r;
    pump_rem_n = pump_rem_r;
    in_state_n = in_state_r;

    case (in_state_r)
      /* IDLE: */
      default: begin
        if (request_valid_i && request_ready_o) begin
          exchange_n = request_data_i[7:0];
          pump_rem_n = request_burst_length_i;
          in_state_n = REQ;
        end
      end

      REQ: begin
        if (spi_req_valid_o && spi_req_ready_i) begin
          exchange_n = '0;
          pump_rem_n = pump_rem_r;
          in_state_n = `MUX_CAST(InputState, (pump_rem_n == 0) ? IDLE : PUMP);
        end
      end

      PUMP: begin
        if (spi_req_valid_o && spi_req_ready_i) begin
          exchange_n = '0;
          pump_rem_n = pump_rem_r - 1;
          in_state_n = `MUX_CAST(InputState, (pump_rem_n == 0) ? IDLE : PUMP);
        end
      end
    endcase
  end
endmodule
