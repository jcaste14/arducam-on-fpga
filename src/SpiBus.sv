// A one-host-to-one-peer SPI module.
// TODO: Extend `nselect_o` to a one-hot bus of select signals.
// TODO: Add `addr_i` to drive the choice of select signal
module SpiBus #(
  parameter  integer LOG_WIDTH = 3,  // 2**3 = 8,
  localparam integer WIDTH = 2 ** LOG_WIDTH
) (
  input  wire logic clk_i,    // <= 8 Mhz
  input  wire logic reset_i,

  // Low-level interface
  output wire logic sclk_o,     // CPOL = 0, CPHA = 0
  output var  logic nselect_o,  // active low
  output wire logic outbound_o,
  input  wire logic inbound_i,

  // High-level interface w/ 4-phase handshake
  input  wire logic valid_i,  // {0 :-> hold,       1 :-> exchange}
  output wire logic ready_o,  // {0 :-> exchanging, 1 :-> idle}
  input  wire logic[WIDTH-1:0] data_i,
  output var  logic[WIDTH-1:0] data_o
);
  // We're ready for data when we're not talking to a peripheral.
  assign ready_o = nselect_o;

  // Manage the lifecycle of an exchange session.
  var logic                           nselect_n;
  var logic[LOG_WIDTH-1:0] counter_r, counter_n;
  always_ff @(posedge clk_i) begin
    nselect_o <= (reset_i) ? '1 : nselect_n;
    counter_r <= (reset_i) ? '0 : counter_n;
  end
  always_comb begin
    if (nselect_o) begin
      // Begin an exchange when we receive valid upstream data.
      counter_n = '0;
      nselect_n = !(ready_o && valid_i);
    end else begin
      // The active exchange is complete once we wrap around to zero.
      counter_n = counter_r + 1;
      nselect_n = (~| counter_n);
    end
  end

  // Exchange bits with the peripheral.
  // SPI parameters {CPOL, CPHA} = 2'b00:
  // - read (into LSB) on rising edge
  // - write (out of MSB) on falling edge
  var inbound_r;
  assign
    sclk_o = (!nselect_o) ? ~clk_i : '0,
    outbound_o = data_o[WIDTH-1:WIDTH-1];
  always_ff @(negedge clk_i)
    inbound_r <= inbound_i;
  always_ff @(posedge clk_i) begin
    if (!nselect_o) begin
      data_o <= {data_o[WIDTH-2:0], inbound_r};
    end else begin
      data_o <= data_i;
    end
  end
endmodule
