module Cpu #(
  parameter integer    ROM_LENGTH = 256,
  parameter /*string*/ ROM_SOURCE = ""
) (
  input  wire logic clk_i,
  input  wire logic reset_i,

  input  wire logic resume_i,
  output wire logic halted_o,
  output wire logic halt_code_o,
  // debugging
  output wire logic[2:0] state_o,

  output wire logic       cam_request_valid_o,
  input  wire logic       cam_request_ready_i,
  output var  logic[15:0] cam_request_data_o,
  output var  logic       cam_request_burst_o,
  output wire logic[23:0] cam_request_burst_length_o,

  input  wire logic       cam_response_valid_i,
  output wire logic       cam_response_ready_o,
  input  wire logic[7:0]  cam_response_data_i
);
  localparam ADDR_WIDTH = $clog2(ROM_LENGTH);
  typedef logic[ADDR_WIDTH-1:0] Address;

  typedef enum logic[2:0] {
    HALT  = 3'b000,
    SLEEP = 3'b001,
    JUMP  = 3'b010,
    COND  = 3'b011,
    PEEK  = 3'b100,
    POKE  = 3'b101,
    WIND  = 3'b110,
    BURST = 3'b111
  } Opcode;

  typedef enum logic[0:0] {
    IMMEDIATE   = 1'b0,
    ACCUMULATOR = 1'b1
  } Addressing;


  wire logic[23:0] insn_r;
  wire Opcode     opcode_r = `MUX_CAST(Opcode, insn_r[23:21]);
  wire Addressing operand_a_addressing_r = `MUX_CAST(Addressing, insn_r[20:20]);
  wire Addressing operand_b_addressing_r = `MUX_CAST(Addressing, insn_r[19:19]);
  var  logic[7:0] operand_a_r;
  var  logic[7:0] operand_b_r;
  always_comb case (operand_a_addressing_r)
    IMMEDIATE:   operand_a_r = insn_r[15:8];
    ACCUMULATOR: operand_a_r = accumulator_r;
  endcase
  always_comb case (operand_b_addressing_r)
    IMMEDIATE:   operand_b_r = insn_r[7:0];
    ACCUMULATOR: operand_b_r = accumulator_r;
  endcase

  typedef enum {
    HALTED,
    SLEEPING,
    ACTIVE,
    CAM_TX,
    CAM_RX
  } CpuState;

  // Internal state
  var CpuState    cpu_state_r,       cpu_state_n;
  var Address     insn_address_r,    insn_address_n;
  var logic[ 7:0] accumulator_r,     accumulator_n;
  var logic[23:0] burst_length_r,    burst_length_n;
  // Outputs
  var logic[15:0] cam_request_data_n;
  var logic       cam_request_burst_n;
  assign
    state_o                    = cpu_state_r,
    cam_request_valid_o        = (cpu_state_r == CAM_TX),
    cam_response_ready_o       = (cpu_state_r == CAM_RX),
    halted_o                   = (cpu_state_r == HALTED),
    cam_request_burst_length_o = burst_length_r,
    halt_code_o                = accumulator_r;
  always_ff @(posedge clk_i) begin
    cpu_state_r    <= `MUX_CAST(CpuState, (reset_i) ? HALTED : cpu_state_n);
    insn_address_r <= (reset_i) ? '0     : insn_address_n;
    accumulator_r  <= (reset_i) ? '0     : accumulator_n;
    burst_length_r <= (reset_i) ? '0     : burst_length_n;

    cam_request_data_o  <= (reset_i) ? '0 : cam_request_data_n;
    cam_request_burst_o <= (reset_i) ? '0 : cam_request_burst_n;
  end
  always_comb begin
    // Defaults
    cpu_state_n    = cpu_state_r;
    insn_address_n = insn_address_r;
    accumulator_n  = accumulator_r;
    burst_length_n = burst_length_r;

    cam_request_data_n  = cam_request_data_o;
    cam_request_burst_n = cam_request_burst_o;

    case (cpu_state_r)
      HALTED: begin
        if (resume_i) begin
          cpu_state_n    = ACTIVE;
          insn_address_n = '0;
        end
      end

      SLEEPING: begin
        if (accumulator_r == 0) begin
          cpu_state_n   = ACTIVE;
        end else begin
          accumulator_n = accumulator_r - 1;
        end
      end

      CAM_TX: begin
        if (cam_request_ready_i) begin
          cam_request_data_n  = '0;
          cam_request_burst_n = '0;
          burst_length_n      = '0;

          case (opcode_r)
            PEEK: begin
              // Wait for a response
              cpu_state_n = CAM_RX;
            end

            POKE, BURST: begin
              // Keep going -- we won't get a response
              cpu_state_n    = ACTIVE;
              insn_address_n = insn_address_r + 1;
            end

            default: begin
              cpu_state_n   = HALTED;
              accumulator_n = 8'hFF;
            end
          endcase
        end
      end

      CAM_RX: begin
        if (cam_response_valid_i) begin
          cpu_state_n    = ACTIVE;
          insn_address_n = insn_address_r + 1;
          accumulator_n  = accumulator_r & cam_response_data_i;
        end
      end

      ACTIVE: case (opcode_r)
        // HALT  [data]
        3'b000: begin
          cpu_state_n    = HALTED;
          accumulator_n  = operand_a_r;
          insn_address_n = insn_address_r + 1;
        end

        // SLEEP [cycles]
        3'b001: begin
          cpu_state_n    = SLEEPING;
          accumulator_n  = operand_a_r;
          insn_address_n = insn_address_r + 1;
        end

        // JUMP  [offset]
        3'b010: begin
          insn_address_n = insn_address_r + operand_a_r;
        end

        // COND  [offset]
        3'b011: begin
          if (accumulator_r) begin
            insn_address_n = insn_address_r + operand_a_r;
          end else begin
            insn_address_n = insn_address_r + 1;
          end
        end

        // PEEK  [addr] [mask]
        3'b100: begin
          cpu_state_n   = CAM_TX;
          accumulator_n = operand_b_r;

          cam_request_data_n  = {operand_a_r & ~8'h80, 8'h00};
          cam_request_burst_n = '0;
        end

        // POKE  [addr] [data]
        3'b101: begin
          cpu_state_n = CAM_TX;

          cam_request_data_n  = {operand_a_r | 8'h80, operand_b_r};
          cam_request_burst_n = '0;
        end

        // WIND  [data]
        3'b110: begin
          burst_length_n = {burst_length_r[15:0], accumulator_r};
          insn_address_n = insn_address_r + 1;
        end

        // BURST SPI Burst Read [addr]
        3'b111: begin
          cpu_state_n = CAM_TX;

          cam_request_data_n  = {operand_a_r & ~8'h80, 8'h00};
          cam_request_burst_n = '1;
        end
      endcase
    endcase
  end

  Firmware #(
    .WIDTH(24),
    .DEPTH(ROM_LENGTH),
    .PRELOAD(ROM_SOURCE)
  ) firmware(
    .clk_i(clk_i),
    .read_addr_i(insn_address_n),
    .read_data_o(insn_r)
  );
endmodule
