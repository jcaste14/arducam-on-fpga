// These two D Flip Flops form what is known as a Synchronizer. We
// will learn about these in Week 5, but you can see more here:
// https://inst.eecs.berkeley.edu/~cs150/sp12/agenda/lec/lec16-synch.pdf
module synchronizer(
  input  wire logic[0:0] clock_i,
  input  wire logic[0:0] signal_n_async_unsafe_i,
  output var  logic[0:0] signal_o
);
  var logic[0:0] signal_n_sync_r;
  always_ff @(posedge clock_i) begin
    signal_n_sync_r <=  signal_n_async_unsafe_i;
    signal_o        <= ~signal_n_sync_r;
  end
endmodule: synchronizer
