module test_top();
  timeunit 1ns;
  timeprecision 1ps;

  localparam integer LOG_WIDTH = 3;  // 2**3 = 8,
  localparam integer WIDTH = 2 ** LOG_WIDTH;
  localparam type    WORD  = logic[WIDTH-1:0];

  var  logic clk_i   = '0;
  var  logic reset_i = '1;
  var  logic error   = '0;

  task pulse();
    #1;
    clk_i = '1;

    #1;
    clk_i = '0;

    if ($time() >= 2000) begin
      error = 1;
      $display("Testbench timed out");
      $finish();
    end
  endtask

  wire logic sclk_o;
  wire logic nselect_o;
  wire logic outbound_o;
  var  logic inbound_i = '1;
  top dut(
    .clk_8mhz_i(clk_i),
    .reset_n_async_unsafe_i(~reset_i),

    // SPI interface
    .sclk_o(sclk_o),
    .nselect_o(nselect_o),
    .outbound_o(outbound_o),
    .inbound_i(inbound_i)
  );

  initial begin
    $dumpfile("simulate.vcd");
    $dumpvars;
    for (int idx = 0; idx < dut.scriptRom.DEPTH; idx++)
      $dumpvars(0, dut.scriptRom.mem[idx]);
    for (int idx = 0; idx < dut.sink.buffer.DEPTH; idx++)
      $dumpvars(0, dut.sink.buffer.mem[idx]);

    // Reset the circuit.
    repeat(1) pulse();
    reset_i = '0;

    repeat(512) pulse();

    $finish();
  end
endmodule
