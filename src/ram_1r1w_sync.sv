module ram_1r1w_sync #(
  parameter integer WIDTH = 8,
  parameter integer DEPTH = 128,
  parameter /*string*/ PRELOAD = ""
) (
  input  logic[0:0] clk_i,
  input  logic[0:0] reset_i,
  
  input  logic[0:0] wr_valid_i,
  input  logic[WIDTH-1:0] wr_data_i,
  input  logic[$clog2(DEPTH) - 1 : 0] wr_addr_i,

  input  logic[$clog2(DEPTH) - 1 : 0] rd_addr_i,
  output logic[WIDTH-1:0] rd_data_o
);
  var logic[WIDTH - 1 : 0] mem[DEPTH - 1 : 0];
  always_ff @(posedge clk_i)
    if (wr_valid_i) mem[wr_addr_i] <= wr_data_i;

  var logic[WIDTH - 1 : 0] rd_result_r;
  always_ff @(posedge clk_i)
    rd_result_r <= mem[rd_addr_i];

  assign rd_data_o = rd_result_r;

  initial begin
    if (PRELOAD != "") $readmemh(PRELOAD, mem, 0, DEPTH - 1);
  end
endmodule
