module Firmware #(
  parameter integer    WIDTH   = 24,
  parameter integer    DEPTH   = 256,
  parameter /*string*/ PRELOAD = "",
  localparam integer   ADDR_WIDTH = $clog2(DEPTH)
) (
  input  wire logic clk_i,
  input  wire logic[ADDR_WIDTH-1:0] read_addr_i,
  output wire logic[WIDTH-1:0] read_data_o
);
  ram_1r1w_sync #(
    .WIDTH(WIDTH),
    .DEPTH(DEPTH),
    .PRELOAD(PRELOAD)
  ) mem(
    .clk_i(clk_i),
    .reset_i('0),

    .wr_valid_i('0),
    .wr_data_i('0),
    .wr_addr_i('0),

    .rd_addr_i(read_addr_i),
    .rd_data_o(read_data_o)
  );
endmodule
