module RomStage #(
  parameter integer    WIDTH = 8,
  parameter integer    DEPTH = 256,
  parameter /*string*/ PRELOAD = ""
) (
  input  wire logic clk_i,
  input  wire logic reset_i,

  output wire logic valid_o,
  input  wire logic ready_i,
  output wire logic[WIDTH - 1 : 0] data_o
);
  typedef logic[WIDTH - 1 : 0] WORD;

  var WORD mem[DEPTH - 1 : 0];
  initial begin
    if (PRELOAD != "") $readmemh(PRELOAD, mem, 0, DEPTH - 1);
  end

  var integer rd_addr_r, rd_addr_n;
  always_ff @(posedge clk_i)
    rd_addr_r <= (reset_i) ? '0 : rd_addr_n;
  always_comb
    rd_addr_n = rd_addr_r + (valid_o & ready_i);

  assign
    valid_o = (rd_addr_r != DEPTH && !reset_i),
    data_o = mem[rd_addr_r];
endmodule
