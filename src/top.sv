module top(
  input wire logic clk_12mhz_i,
  // n: Negative Polarity (0 when pressed, 1 otherwise)
  // async: Not synchronized to clock
  // unsafe: Not De-Bounced
  input wire logic reset_n_async_unsafe_i,

  // SPI interface
  output wire logic spi_sclk_o,     // CPOL = 0, CPHA = 0
  output var  logic spi_nselect_o,  // active low
  output var  logic spi_outbound_o,
  input  wire logic spi_inbound_i,

  // UART interface
  input  wire logic uart_rx_i,
  output wire logic uart_tx_o,

  // LED interface
  output wire logic led_c_o,
  output wire logic led_n_o,
  output wire logic led_s_o,
  output wire logic led_e_o,
  output wire logic led_w_o
);
  var logic clk_6mhz_r;
  always_ff @(posedge clk_12mhz_i)
    clk_6mhz_r = ~clk_6mhz_r;
  wire logic clk_6mhz_i = clk_6mhz_r;

  wire logic reset_i;
  synchronizer sync(
    .clock_i(clk_6mhz_i),
    .signal_n_async_unsafe_i(reset_n_async_unsafe_i),
    .signal_o(reset_i)
  );

  wire logic      uart_valid_r;
  wire logic      uart_ready_r;
  wire logic[7:0] uart_data_r;
  UartSink #(
    .CLOCK_FREQ(6_000_000),
    .BAUD_RATE(115_200)
  ) uartSink(
    .clk_i(clk_6mhz_i),
    .reset_i(reset_i),

    // Low-level peripheral interface
    .tx_o(uart_tx_o),

    // High-level pipeline interface
    .data_i(uart_data_r),
    .valid_i(uart_valid_r),
    .ready_o(uart_ready_r)
  );

  wire logic      spi_req_valid_r;
  wire logic      spi_req_ready_r;
  wire logic[7:0] spi_req_data_r;
  //
  wire logic      spi_res_valid_r;
  wire logic      spi_res_ready_r;
  wire logic[7:0] spi_res_data_r;
  //
  SpiStage #(
    .LOG_WIDTH(3)
  ) spi(
    .clk_i(clk_6mhz_i),
    .reset_i(reset_i),

    .sclk_o(spi_sclk_o),
    .nselect_o(spi_nselect_o),
    .outbound_o(spi_outbound_o),
    .inbound_i(spi_inbound_i),

    .valid_i(spi_req_valid_r),
    .ready_o(spi_req_ready_r),
    .data_i(spi_req_data_r),

    .valid_o(spi_res_valid_r),
    .ready_i(spi_res_ready_r),
    .data_o(spi_res_data_r)
  );

  /**
   * This component controls the camera peripheral directly.
   * It supports single-write, single-read, and burst-read interactions,
   * and outputs burst data over a separate interface.
  **/
  wire logic[15:0] cam_req_data_r;
  wire logic       cam_req_burst_r;
  wire logic[23:0] cam_req_burst_length_r;
  wire logic       cam_req_valid_r;
  wire logic       cam_req_ready_r;
  //
  wire logic[7:0]  cam_res_data_r;
  wire logic       cam_res_valid_r;
  wire logic       cam_res_ready_r;
  //
  CameraController cam(
    .clk_i(clk_6mhz_i),
    .reset_i(reset_i),

    .spi_req_valid_o(spi_req_valid_r),
    .spi_req_ready_i(spi_req_ready_r),
    .spi_req_data_o(spi_req_data_r),

    .spi_res_valid_i(spi_res_valid_r),
    .spi_res_ready_o(spi_res_ready_r),
    .spi_res_data_i(spi_res_data_r),

    .request_data_i(cam_req_data_r),
    .request_burst_i(cam_req_burst_r),
    .request_burst_length_i(cam_req_burst_length_r),
    .request_valid_i(cam_req_valid_r),
    .request_ready_o(cam_req_ready_r),

    .response_data_o(cam_res_data_r),
    .response_valid_o(cam_res_valid_r),
    .response_ready_i(cam_res_ready_r),

    .burst_data_o(uart_data_r),
    .burst_valid_o(uart_valid_r),
    .burst_ready_i(uart_ready_r)
  );

  /**
   * This CPU executes preloaded firmware to drive the SPI controller.
   * By changing the ROM_SOURCE file, different camera applications
   * can be developed using the same hardware design.
  **/
  var  logic       cpu_resume_r;
  wire logic       cpu_halted_r;
  wire logic       cpu_halt_code_r;
  wire logic[2:0]  cpu_state_o;
  //
  Cpu #(
    .ROM_LENGTH(256),
    .ROM_SOURCE("src/script.hex")
  ) cpu(
    .clk_i(clk_6mhz_i),
    .reset_i(reset_i),

    .resume_i(cpu_resume_r),
    .halted_o(cpu_halted_r),
    .halt_code_o(cpu_halt_code_r),
    .state_o(cpu_state_o),

    .cam_request_valid_o(cam_req_valid_r),
    .cam_request_ready_i(cam_req_ready_r),
    .cam_request_data_o(cam_req_data_r),
    .cam_request_burst_o(cam_req_burst_r),
    .cam_request_burst_length_o(cam_req_burst_length_r),

    .cam_response_valid_i(cam_res_valid_r),
    .cam_response_ready_o(cam_res_ready_r),
    .cam_response_data_i(cam_res_data_r)
  );

  // high for one cycle after reset_i falls
  always_ff @(posedge clk_6mhz_r)
    cpu_resume_r <= (reset_i) ? '1 : '0;

  // Prevent the LEDs from floating halfway...
  assign
    led_c_o = cpu_state_o[2:2],
    led_n_o = cpu_state_o[1:1],
    led_s_o = cpu_state_o[0:0],
    led_e_o = '0,
    led_w_o = '0;
endmodule
