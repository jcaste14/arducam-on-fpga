module test_SpiBus();
  timeunit 1ns;
  timeprecision 1ps;

  localparam integer LOG_WIDTH = 3;  // 2**3 = 8,
  localparam integer WIDTH = 2 ** LOG_WIDTH;
  localparam type    WORD  = logic[WIDTH-1:0];

  var  logic clk_i   = '0;
  var  logic reset_i = '1;
  var  logic error   = '0;

  task pulse();
    #1;
    clk_i = '1;

    #1;
    clk_i = '0;

    if ($time() >= 2000) begin
      error = 1;
      $display("Testbench timed out");
      $finish();
    end
  endtask

  // Low-level interface
  wire logic sclk_o;
  wire logic nselect_o;
  wire logic outbound_o;
  var  logic inbound_i = '0;

  // High-level interface
  var  logic valid_i = '0;
  wire logic ready_o;
  var  WORD  data_i;
  wire WORD  data_o;
  SpiBus#(LOG_WIDTH) dut(
    .clk_i(clk_i),
    .reset_i(reset_i),

    // Low-level interface
    .sclk_o(sclk_o),
    .nselect_o(nselect_o),
    .outbound_o(outbound_o),
    .inbound_i(inbound_i),

    // High-level interface
    .valid_i(valid_i),
    .ready_o(ready_o),
    .data_i(data_i),
    .data_o(data_o)
  );

  initial begin
    $dumpfile("simulate.vcd");
    $dumpvars;

    // Reset the circuit.
    repeat(2) pulse();
    reset_i = '0;

    // Pump a byte of data through the interface.
    data_i  = 8'hAB;
    valid_i = '1;
    pulse();

    data_i  = 'x;
    valid_i = '0;
    repeat(8) pulse();

    data_i  = 8'hCD;
    valid_i = '1;
    pulse();

    data_i  = 'x;
    valid_i = '0;
    repeat(8) pulse();

    $finish();
  end
endmodule
