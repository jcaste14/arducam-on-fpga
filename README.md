**CAVEAT EMPTOR**: While I put a hell of a lot of time into this project, I was ultimately unable to get it tested and working on the physical FPGA by the submission deadline. I can only certify that `make prog` doesn't outright fail to program the device, and that the `make test-spi` waveform test (see below) produces waveforms that look correct.

There is a list of lessons learned at the bottom of this file, which I recommend as a kind of retrospective given the difficulties I had with this project.

## Building and programming

Clone the repository with `git clone git@git.ucsc.edu:jcaste14/arducam-on-fpga.git`. GitHub and GitLab both provide affordances to help with this.

Run `make prog` to build all required modules with Yosys and program a device. If you only want to build the modules for simulation, use `make top`.

## Testing

The `SpiBus.sv` module includes a waveform test. Run `make test-spi` followed by `./simulate`, then open the generated `simulate.vcd`. Load the view file `test-SpiBus.gtkw` and visually inspect the waveform for correctness.

## Application design

All SystemVerilog sources can be found in the `src/` subdirectory.

This application breaks down into four large-scale modules: an SPI exchanger, a camera controller, a CPU for scripting the camera controller, and a terminal UART. The camera controller sits in the middle of all of these, converting high-level commands from the CPU into sequences of SPI exchanges and piping image data out to the UART.

* The `SpiBus` module is a low-level building block for performing word exchanges with a periperhal. The slightly higher-level `SpiStage` module adapts it to a standard pair of ready-valid-and pipeline interfaces.

* The `CameraController` module is a high-level state machine for performing the structured sequences of SPI exchanges supported by an ArduCam. It can perform Single Write, Single Read, and Burst Read interactions on upstream request. This module has two downstream interfaces: one for conveying single-byte responses, typically back around to an upstream module, and the other for processing multi-byte burst payloads.

  This module is fairly complex, but it factors into two state machines: an input-generation state machine, which pumps the SPI bus with as many input words as are necessary, and an output-processing state machine, which transfers each output byte to its rightful destination. Each state machine organizes its logic into two behavioral blocks: one for routing and one for changing states. This seems to make the whole thing a little more comprehensible.

* The `Cpu` module implements a general-purpose state machine for directing the particular interactions undertaken by a `CameraController`. Properly, it implements a very small custom ISA with instructions for `PEEK`, `POKE`, and `BURST`, corresponding to the three SPI flows mentioned above, together with a handful of other instructions.

  The `script.hex` file serves as the firmware for this CPU, and it also documents the ISA for an assembly author's use. I found it easier to write this script and build a CPU to implement it than to write a fixed-function state machine; you can blame the software rot that has set into my brain over 20 years.

* All of the above are integrated into a `top` module, which also divides
  the clock down to a safe frequency for the ArduCam's SPI capabilities.

## Third-party modules

* The `UartBaudTickGenerator` and `UartSink` modules included in this
  repository are gratefully sourced from
  https://github.com/icebreaker-fpga/icebreaker-verilog-examples/tree/main/icebreaker/pll_uart .

* The `ram_1r1w_sync.sv` module was developed in an earlier CSE 225 lab,
  and the `synchronizer.sv` was adapted from logic provided in lab skeletons.

## Retrospective and lessons learned

* This project was a lot of fun, but I also found that my relative hardware
  illiteracy lead to a lot of "unknown unknowns" that I struggled to notice
  and then work my way out of. If I had more time between my graduate
  research and other projects, this would have been the perfect-sized
  project for me to use to learn those "unknown unknowns" from first
  principles (which is how I prefer to learn). Unfortunately, I found myself
  a bit overwhelmed; I can only hope the effort I put in is evident.

* The Lattice iCE40 includes hardware SPI blocks, and a vendor-provided
  Module Generator can be used to generate SPI interface modules that
  make use of these blocks.

  I don't think there is much harm in using my own SPI module -- in
  fact, I suspect it might be particularly important for simulation --
  but in the future, it would probably be good to make use of the
  pre-existing hardwired logic instead of wasting LUTs and whatnot.

* It took me far too long to realize that the camera module needs a 5V
  power supply, rather than the 3.3V provided by the PMODs. I feel very
  lucky that the iCEBreaker kit included a two-prong header I could
  use to route the relevant pins to one of the on-board 5V supplies.
  Unfortunately, I still needed to solder that header in, and I am not
  very confident in my soldering skills yet...

* Similarly, I only realized very late that the ArduCam modules used
  in my project output entire JPEG files rather than raw pixel data.
  While the original goal of the project was to display a photo using
  a connected display peripheral, this discovery lead me to instead
  exfiltrate the generated JPEG files to an external computer using
  the iCEBreaker's UART pins. (Thank goodness the UART piggybacks over
  the programming/power cable.)

* While most of `Cpu.sv` was not particularly difficult to writ once I'd
  decided to push the camera application logic into custom firmware, it
  still took me a while to really comprehend how to arrange for the CPU
  to pause during I/O actions. It ended up not being very complex, but
  that's directly because I pushed all of the real complexity out to a
  helper module, `CameraController.sv`.

* The `CameraController.sv` module really stretched my hardware abilities,
  and it is easily the most complex module I've written so far. Where the
  `SpiBus.sv` module manages the exchange of a single word, and th
  `SpiStage.sv` module manages the flow of arbitrary words in and out of the
  underlying SPI bus, the `CameraController.sv` module manages structured,
  multi-word exchange sessions. It consists of two related state machines,
  one to generate input for the SPI bus and one to handle the output from
  the SPI bus. Splitting the logic into "how are the wires routed in this
  state" vs. "what data do the wires carry in this state" helped me
  immensely. In the end, I think this module ended up surprisingly readable.

* Icarus Verilog and Yosys have mutually incompatible issues around ternary
  expressions and `typedef`-defined types. I suspect that this is only the tip
  of the iceberg as far as SystemVerilog implementation differences are
  concerned. For this particular issue, I added a macro to `macros.sv`
  so that conditional compilation will present each compiler with what it
  expects. I'd love to better understand how these kinds of discrepancies
  are managed in industry, though.
